<?php

/**
 * Helper module for handling inline WYSIWYG images in body fields.
 * By default these are inserted using relative paths. We pick them up
 * and make sure the files gets attached properly by Swiftmailer module.
 *
 * Waarning: currently only tested for Simplenews / Emogrifier
 */

use Drupal\Component\Utility\Unicode;

/**
 * Implements hook_mail_alter().
 */
function swiftmailer_inline_images_mail_alter(&$message) {
  if (!is_array($message['body'])) {
    return;
  }

  // Mail body is an array, we go through each
  foreach ($message['body'] as &$bodyitem) {
    // Make sure we are only processing body items that
    // use markuptrait -> they have the create method
    if (!method_exists($bodyitem, 'create')) {
      return;
    }

    $body_text = (string) $bodyitem;
    $pattern = '/src="(\/[^"]+\.gif|\/[^"]+\.jpg|\/[^"]+\.png|\/[^"]+\.GIF|\/[^"]+\.JPG|\/[^"]+\.PNG)"/';

    $embeddable_images = array();
    $processed_images = array();
    preg_match_all($pattern, $body_text, $embeddable_images);
    for ($i = 0; $i < count($embeddable_images[0]); $i++) {
      $image_uri = $embeddable_images[1][$i];
      if (isset($processed_images[$image_uri])) {
        continue;
      }
      $image_path = $image_uri;
      $image_path = trim($image_path);
      $image_name = basename($image_path);
      $image_path = Unicode::substr($image_path, 1);
      $image = new \stdClass();
      $image->uri = $image_path;
      $image->filename = $image_name;
      $image->filemime = \Drupal::service('file.mime_type.guesser')
        ->guess($image_path);
      $image->cid = rand(0, 9999999999);
      $message['params']['images'][] = $image;
      $body_text = str_replace($image_uri, 'cid:' . $image->cid, $body_text);
      $processed_images[$image_path] = 1;
    }
    if (!empty($processed_images)) {
      $bodyitem = $bodyitem::create($body_text);
    };
  }
}